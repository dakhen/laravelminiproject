<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Company;
use DB;
use Carbon;

class CompanyController extends Controller
{
    public function index(){
        $data['companylists'] = Company::all();
        return view('company.company',$data);
    }

    public function allCompany(){
        $data['companies'] = Company::all();
        return view('company.companynoauth', $data);
    }

    public function createCompany(){
        return view('company.createcompany');
    }

    public function saveCompany(Request $req){
        $data = array(
            'company_name' => $req->companyname,
            'company_type' => $req->companytype,
            'company_emp_number'=> $req->companyemp,
            'company_website'=> $req->companywebsite,
            'company_contact' => $req->companycontact,
            'created_at' => Carbon\Carbon::now()->format('Y-m-d'),
            'updated_at' => Carbon\Carbon::now()->format('Y-m-d'),
        );

        $i = DB::table('tbl_companies')->insert($data);
        if($i)
        {
            $req->session()->flash('success', 'Data has been saved!');
            return redirect('/company/companylist');
        }
        else{
            $req->session()->flash('error','Fail to save data, please check again!');
            return redirect('/company/create')->withInput();
        }
    }

    public function companyDetail($id){
        $data['company'] = DB::table('tbl_companies')
        ->where('id', $id)
        ->first();
        return view('company.companydetail', $data);
    }

    public function deleteCompany(Request $r)
    {
        Company::find($r->id)
            ->delete();
        $r->session()->flash('success', 'Data has been removed!');
        return redirect('/company/companylist');
    }

    public  function editCompany($id){

        $data['company'] = DB::table('tbl_companies')
        ->where('id', $id)
        ->first();
        return view('company.updatecompany', $data);
    }

    public function updateCompany(Request $req)
    {
        $data = array(
            'company_name' => $req->companyname,
            'company_type' => $req->companytype,
            'company_emp_number'=> $req->companyemp,
            'company_website'=> $req->companywebsite,
            'company_contact' => $req->companycontact,
            'updated_at' => Carbon\Carbon::now()->format('Y-m-d'),
        );
        
        $i = Company::find($req->id)
            ->update($data);

        if($i)
        {
            $req->session()->flash('success', 'Data has been saved!');
            return redirect('/company/companylist');
        }
        else{
            $req->session()->flash('error','Fail to save data, please check again!');
            return redirect('/company/create')->withInput();
        }
    }

}
