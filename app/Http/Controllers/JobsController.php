<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Job;
use DB;
use Carbon;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function index()
    {
        $data['joblists'] =  Job::all();
        // ->select('tbl_jobs.*')
        // ->get(); it get all
        // ->paginate(10);
        return view('job.job',$data);
    }

    public function allJob()
    {
        $data['joblists'] =  Job::all();
        // ->select('tbl_jobs.*')
        // ->get(); it get all
        // ->paginate(10);
        return view('job.jobnoauth',$data);
    }

    public function createJob()
    {
        // $data['categories'] = DB::table('tbl_categories')
        //     ->get();
        // $data['companies'] = DB::table('tbl_companyies')
        // ->get();
        return view('job.createjob');
    }

    public function saveJob(Request $c)
    {
        // $data = $r->except('_token', 'cpass');
        // print_r($data);
        $data = array(
            'job_name' => $c->jobname,
            'job_description' => $c->jd,
            'job_requirement' => $c->jr,
            'job_location'=> $c->joblocation,
            'job_salary' => $c->jobsalary,
            'close_date'=> $c->jbclosedate,
            'created_at' => Carbon\Carbon::now()->format('Y-m-d'),
            'updated_at' => Carbon\Carbon::now()->format('Y-m-d'),
        );
      
        $i = DB::table('tbl_jobs')->insert($data);
        if($i)
        {
            $c->session()->flash('success', 'Data has been saved!');
            return redirect('/job/joblist');
        }
        else{
            $c->session()->flash('error','Fail to save data, please check again!');
            return redirect('/job/create')->withInput();
        }
    }

    public function detailJob($id){
        $data['job'] = DB::table('tbl_jobs')
        ->where('id', $id)
        ->first();
        return view('job.jobdetail', $data);
    }

    public function deleteJob(Request $r)
    {
        Job::find($r->id)
            ->delete();
        $r->session()->flash('success', 'Data has been removed!');
        return redirect('/job/joblist');
    }

    public  function editJob($id){

        $data['job'] = DB::table('tbl_jobs')
        ->where('id', $id)
        ->first();
        return view('job.updatejob', $data);
    }

    public function updateJob(Request $c)
    {
        $data = array(
            'job_name' => $c->jobname,
            'job_description' => $c->jd,
            'job_requirement' => $c->jr,
            'job_location'=> $c->joblocation,
            'job_salary' => $c->jobsalary,
            'close_date'=> $c->jbclosedate,
            'updated_at' => Carbon\Carbon::now()->format('Y-m-d'),
        );
        
        $i = Job::find($c->id)
            ->update($data);

        if($i)
        {
            $c->session()->flash('success', 'Data has been saved!');
            return redirect('/job/joblist');
        }
        else{
            $c->session()->flash('error','Fail to save data, please check again!');
            return redirect('/job/create')->withInput();
        }
    }

}
