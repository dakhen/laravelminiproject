<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use DB;
use Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['userlists'] =  User::all();
        // ->select('tbl_jobs.*')
        // ->get(); it get all
        // ->paginate(10);
        return view('user.user',$data);
    }

    // public function allUser()
    // {
    //     $data['userlists'] =  User::all();
    //     // ->select('tbl_jobs.*')
    //     // ->get(); it get all
    //     // ->paginate(10);
    //     return view('user.user',$data);
    // }

    public function saveUser(Request $c)
    {
        // $data = $r->except('_token', 'cpass');
        // print_r($data);
        $data = array(
            'name' => $c->username,
            'email' => $c->email,
        );
      
        $i = DB::table('users')->insert($data);
        if($i)
        {
            $c->session()->flash('success', 'User has been saved!');
            return redirect('/user/userlist');
        }
        else{
            $c->session()->flash('error','Fail to save user, please check again!');
            return redirect('/user/create')->withInput();
        }
    }

    public function editUser($id){

        $data['user'] = User::find($id)
        ->first();
        return view('user.updateuser', $data);
    }
    public function updateUser(Request $c)
    {
        $data = array(
            'name' => $c->username,
            'email' => $c->email,
        );
        
        $i = User::find($c->id)
            ->update($data);

        if($i)
        {
            $c->session()->flash('success', 'User has been saved!');
            return redirect('/user/userlist');
        }
        else{
            $c->session()->flash('error','Fail to save user, please check again!');
            return redirect('/user/create')->withInput();
        }
    }

    public function userdetail($id){
        $data['user'] = User::find($id)
        ->first();
        return view('user.userdetail', $data);
    }

    public function deleteUser(Request $r)
    {
        User::find($r->id)
            ->delete();
        $r->session()->flash('success', 'User has been removed!');
        return redirect('/user/userlist');
    }

    

}
