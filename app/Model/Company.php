<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'tbl_companies';

    protected $fillable = [
        'company_name', 'company_type', 'company_emp_number','company_website','company_contact'
    ];
}
