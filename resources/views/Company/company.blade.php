@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div  class="card">
                <div class="card-header">{{ __('Company Table') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>
                        <a href="{{ url('/company/createcompany')}}"  class="btn btn-success">Create New</a>
                    </div>
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Company Name</th>
                            <th scope="col">Company Type</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if(!$pagex)
                                $pagex = 1;
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($companylists as $com)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$com->company_name}}</td>
                                    <td>{{$com->company_type}}</td>
                                    <td>
                                        <a href="{{url('company/companydetail/'.$com->id)}}" class="text-primary" title="Company Detail">
                                            <span><i class="fa fa-info-circle" aria-hidden="true"></i> </span>
                                        </a>
                                        <a href="{{url('company/edit/'.$com->id)}}" class="text-success" title="Update Company">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{url('company/deletecompany?id='.$com->id)}}" class="text-danger" title="Delete Company"
                                        onclick="return confirm('Are you sure to delete?')">
                                         
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
