@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-gray">
                    <div class="card-block">
                        <h5> <b>Update Company</b></h5>
                        <hr>
                        <form action="{{ url('/company/update') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-9">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <p>
                                                {{ session('success') }}
                                            </p>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <p>
                                                {{ session('error') }}
                                            </p>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        <label for="companyname" class="col-sm-4">Company Name
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="companyname" name='companyname' required
                                                autofocus value="{{ $company->company_name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="companytype" class="col-sm-4">Company Type
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea type="text" class="form-control" id="companytype" name='companytype'
                                                required>{{ $company->company_type }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="companyemp" class="col-sm-4">Company Employee Number
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea type="text" class="form-control" id="companyemp" name='companyemp'
                                                required>{{ $company->company_emp_number }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="companywebsite" class="col-sm-4">Company Website
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="companywebsite" name='companywebsite'
                                                required value="{{ $company->company_website }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="companycontact" class="col-sm-4">Company Contact
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="companycontact" name='companycontact' required
                                                value="{{ $company->company_contact }}">
                                        </div>
                                    </div>
                                   
                                    <div style="display: none">
                                        <input type="text" name="id" id="id" value="{{ $company->id}}">
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4"></label>
                                        <div class="col-sm-8">
                                            <button class="btn btn-primary btn-oval">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <a href="{{url('/company/companylist')}}" class="btn btn-primary btn-oval">
                                                <i class="fa fa-reply"></i> Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
