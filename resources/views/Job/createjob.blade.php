@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card card-gray">
            <div class="card-block">
                <h5 style="text-align: center;"><b>Post New Job</b></h5>
                <hr>
                <form action="{{url('/job/save')}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p>
                                    {{session('success')}}
                                </p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                            @if(Session::has('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <p>
                                    {{session('error')}}
                                </p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                            <div class="form-group row">
                                <label for="jobname" class="col-sm-4">Job Title 
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="jobname" 
                                        name='jobname' required autofocus value="{{old('jobname')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jd" class="col-sm-4">Job Description 
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" id="jd" 
                                        name='jd' required value="{{old('jd')}}"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jr" class="col-sm-4">Job Requirement 
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" id="jr" 
                                        name='jr' required value="{{old('jr')}}"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="joblocation" class="col-sm-4">Job Location 
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="joblocation" 
                                        name='joblocation' required value="{{old('joblocation')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jobsalary" class="col-sm-4">Job Salary 
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="jobsalary" 
                                        name='jobsalary' required value="{{old('jobsalary')}}">
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label for="jobcreatedate" class="col-sm-4">Create Date 
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" id="jobcreatedate" 
                                        name='jbcreatedate' required value="{{old('jobcreatedate')}}">
                                </div>
                            </div> --}}
                            <div class="form-group row">
                                <label for="jbclosedate" class="col-sm-4">Close Date 
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" id="jbclosedate" 
                                        name='jbclosedate' required value="{{old('jbclosedate')}}">
                                </div>
                            </div>
                        
                            <div class="form-group row">
                                <label class="col-sm-4"></label>
                                <div class="col-sm-8">
                                    <button class="btn btn-primary btn-oval">
                                        <i class="fa fa-save"></i> Save
                                    </button>
                                    <a href="{{url('/job/joblist')}}" class="btn btn-primary btn-oval">
                                        <i class="fa fa-reply"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
      $("#sidebar-menu li ul li").removeClass("active");
      
            $("#menu_company").addClass("active open");
      $("#company_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
      
        });
        function loadPhoto(e)
        {
            var img = document.getElementById('img');
            img.src = URL.createObjectURL(e.target.files[0]);
        }
    </script>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
      $("#sidebar-menu li ul li").removeClass("active");
      
            $("#menu_company").addClass("active open");
      $("#company_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
      
        });
        function loadPhoto(e)
        {
            var img = document.getElementById('img');
            img.src = URL.createObjectURL(e.target.files[0]);
        }
    </script>
@endsection