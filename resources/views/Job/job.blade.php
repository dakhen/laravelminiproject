@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div  class="card">
                <div class="card-header">{{ __('Job Table') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Job Name</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagex = @$_GET['page'];
                            if(!$pagex)
                                $pagex = 1;
                            $i = 2 * ($pagex - 1) + 1;
                            ?>
                            @foreach ($joblists as $job)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$job->job_name}}</td>
                                    <td>
                                        <a href="{{url('job/jobdetail/'.$job->id)}}" class="text-primary" title="Job Detail">
                                            <span><i class="fa fa-info-circle" aria-hidden="true"></i> </span>
                                        </a>
                                        <a href="{{url('job/edit/'.$job->id)}}" class="text-success" title="Update Job">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="{{url('job/deletejob?id='.$job->id)}}" class="text-danger" title="Delete Job"
                                        onclick="return confirm('Are you sure to delete?')">
                                         
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
