@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <section class="margin-section-content">
                    <div class="container">
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <h6><b>{{ $job->job_name }}</b></h6>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-border-none">
                                    <div class="card-header">Company Profile</div>
                                    <div class="card-body">
                                        {{-- <div class="row">
                                            <div class="col-md-3">
                                                <span>Company</span>
                                            </div>
                                            <div class="col-md-5 text-left text-primary">
                                                <span id="">{{ $job->company_name }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br> --}}
                                        {{-- <div class="row">
                                            <div class="col-md-3">
                                                <span>Function</span>
                                            </div>
                                            <div class="col-md-5 text-left">
                                                <span id="">{{ $job->company_type }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br> --}}
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span>Location</span>
                                            </div>
                                            <div class="col-md-5 text-left">
                                                <span id="">{{ $job->job_location }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span>Salary</span>
                                            </div>
                                            <div class="col-md-5 text-left">
                                                <span id="">{{ $job->job_salary }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br>
                                        {{-- <div class="row">
                                            <div class="col-md-3">
                                                <span>Employees</span>
                                            </div>
                                            <div class="col-md-5 text-left">
                                                <span id="">{{ $job->company_num_emp }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br> --}}
                                        {{-- <div class="row">
                                            <div class="col-md-3">
                                                <span>Create Date</span>
                                            </div>
                                            <div class="col-md-5 text-left">
                                                <span id="">{{ $job->created_date }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br> --}}
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span>Close Date</span>
                                            </div>
                                            <div class="col-md-5 text-left">
                                                <span id="">{{ $job->close_date }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-border-none">
                                    <div class="card-header">Job Description</div>
                                    <div class="card-body">{{ $job->job_description }}</div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-border-none">
                                    <div class="card-header">Job Requirements</div>
                                    <div class="card-body">{{ $job->job_requirement }}</div>
                                </div>
                            </div>
                        </div><br>
                        {{-- <div class="row">
                            <div class="col-md-12">
                                <div class="card card-border-none">
                                    <div class="card-header">Contact Information</div>
                                    <div class="card-body">
                                        {{ $job->company_contact }}
                                    </div>
                                </div>
                            </div>
                        </div><br> --}}


                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
