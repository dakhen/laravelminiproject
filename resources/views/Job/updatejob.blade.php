@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-gray">
                    <div class="card-block">
                        <h5> <b>Update Job</b></h5>
                        <hr>
                        <form action="{{ url('/job/update') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-9">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <p>
                                                {{ session('success') }}
                                            </p>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <p>
                                                {{ session('error') }}
                                            </p>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        <label for="jobname" class="col-sm-4">Job Title
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="jobname" name='jobname' required
                                                autofocus value="{{ $job->job_name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="jd" class="col-sm-4">Job Description
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea type="text" class="form-control" id="jd" name='jd'
                                                required>{{ $job->job_description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="jr" class="col-sm-4">Job Requirement
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <textarea type="text" class="form-control" id="jr" name='jr'
                                                required>{{ $job->job_description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="joblocation" class="col-sm-4">Job Location
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="joblocation" name='joblocation'
                                                required value="{{ $job->job_location }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="jobsalary" class="col-sm-4">Job Salary
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="jobsalary" name='jobsalary' required
                                                value="{{ $job->job_salary }}">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="jbclosedate" class="col-sm-4">Close Date
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="jbclosedate" name='jbclosedate'
                                                required value="{{ $job->close_date }}">
                                        </div>
                                    </div>
                                   
                                    <div style="display: none">
                                        <input type="text" name="id" id="id" value="{{ $job->id}}">
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4"></label>
                                        <div class="col-sm-8">
                                            <button class="btn btn-primary btn-oval">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <a href="{{url('/job/joblist')}}" class="btn btn-primary btn-oval">
                                                <i class="fa fa-reply"></i> Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
