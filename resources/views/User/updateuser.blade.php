@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-gray">
                    <div class="card-block">
                        <h5> <b>Update User</b></h5>
                        <hr>
                        <form action="{{ url('/user/update') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-9">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <p>
                                                {{ session('success') }}
                                            </p>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <p>
                                                {{ session('error') }}
                                            </p>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        <label for="usename" class="col-sm-4">User Name
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="username" name='username' required
                                                autofocus value="{{ $user->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="jd" class="col-sm-4">User Email
                                            <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="email" name='email' required
                                                autofocus value="{{ $user->email }}">
                                        </div>
                                    </div>
                                   
                                    <div style="display: none">
                                        <input type="text" name="id" id="id" value="{{ $user->id}}">
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4"></label>
                                        <div class="col-sm-8">
                                            <button class="btn btn-primary btn-oval">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                            <a href="{{url('/user/userlist')}}" class="btn btn-primary btn-oval">
                                                <i class="fa fa-reply"></i> Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
