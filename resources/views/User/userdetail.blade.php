@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <section class="margin-section-content">
                    <div class="container">
                        <br>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-border-none">
                                    <div class="card-header">User Detail</div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span>User Name</span>
                                            </div>
                                            <div class="col-md-5 text-left text-primary">
                                                <span id="">{{ $user->name }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br> 
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span>Email</span>
                                            </div>
                                            <div class="col-md-5 text-left">
                                                <span id="">{{ $user->email }}</span>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div><br> 
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
