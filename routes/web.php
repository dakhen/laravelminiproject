<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('job')->group(function() {
    Route::get('alljobs', 'JobsController@allJob');
    Route::get('joblist', 'JobsController@index');
    Route::get('createjob', 'JobsController@createJob');
    Route::post('save', 'JobsController@saveJob');
    Route::get('jobdetail/{id}', 'JobsController@detailJob');
    Route::get('edit/{id}', 'JobsController@editJob');
    Route::post('update', 'JobsController@updateJob');
    Route::get('deletejob', 'JobsController@deleteJob');
});

Route::prefix('user')->group(function(){
    Route::get('userlist', 'UserController@index');
    // Route::get('allUser', 'UserController@allUser');
    Route::get('createuser', 'JobsController@createUser');
    Route::post('save', 'UserController@saveUser');
    Route::get('userdetail/{id}', 'UserController@userdetail');

    Route::get('edit/{id}', 'UserController@editUser');
    Route::post('update', 'UserController@updateUser');
    Route::get('deleteUser', 'UserController@deleteUser');
});


Route::prefix('company')->group(function(){
    Route::get('companylist', 'CompanyController@index');
    Route::get('allcompanys', 'CompanyController@allCompany');
    Route::get('createcompany', 'CompanyController@createCompany');
    Route::post('save', 'CompanyController@saveCompany');
    Route::get('companydetail/{id}', 'CompanyController@companyDetail');

    Route::get('edit/{id}', 'CompanyController@editCompany');
    Route::post('update', 'CompanyController@updateCompany');
    Route::get('deletecompany', 'CompanyController@deleteCompany');
});